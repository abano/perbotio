## **Install**
Clone this repository and then run:

    npm install

Once everything is installed run it with:

    npm start


## **Notes**
If the bot is not configured it's not going to work. Just go to the gear on the bottom left to configure your account.

Password is saved encrypted.
