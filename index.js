const logger = require('./libs/logger.js');
const perbotio = require('./perbotio.js');


if( process.argv.length >= 3 ){
	bot = process.argv[2];
} else {
	logger.error('You have to specify the robot to use: npm start [robot] [headless]');
	process.exit();
}

var headlessMode = true;
if( process.argv.length == 4 ){
	headlessMode = false;
}

(async() => {
	const robot = new perbotio(bot, headlessMode);
	await robot.run();

	logger.debug('exit');
	process.exit();
})();