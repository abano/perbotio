document.addEventListener('DOMContentLoaded', (event) => {
	// tooltips
	var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
	var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
		return new bootstrap.Tooltip(tooltipTriggerEl)
	});

	// password reveal
	var passwordRevealTriggerList = [].slice.call(document.querySelectorAll('.password-reveal span'));
	passwordRevealTriggerList.map(function (passwordRevealEl) {
		return passwordRevealEl.addEventListener('click', revealPassword);
	});

	// scrapers run buttons
	var scraperTriggerList = [].slice.call(document.querySelectorAll('.runScraper'));
	scraperTriggerList.map(function (scrapperEl) {
		return scrapperEl.addEventListener('click', app.runScraper);
	});

	// config form
	document.querySelector('#personioUser')?.addEventListener('submit', app.saveConfig);
});


function revealPassword(e) {
	var button = e.target;
	if (button.nodeName === 'I') {
		button = button.parentNode;
	}

	var input = button.parentNode.querySelector('input');
	if (input.type === 'text') {
		input.type = 'password';
		button.querySelector('i').classList.remove('fa-eye');
		button.querySelector('i').classList.add('fa-eye-slash');
	} else if (input.type === 'password') {
		input.type = 'text';
		button.querySelector('i').classList.remove('fa-eye-slash');
		button.querySelector('i').classList.add('fa-eye');
	}
}

