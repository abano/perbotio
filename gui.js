const ipcMain = require('electron').ipcMain;
const path = require('path');
const{ app, BrowserWindow, Menu, Tray} = require('electron');
const perbotio = require('./perbotio.js');

let win;
let tray;

function createWindow () {
	win = new BrowserWindow({
		width: 800,
		height: 600,
		webPreferences: {
			contextIsolation: false,
			nodeIntegration: true
		}
	});

	win.setMenu(null);

	var startingPoint = path.resolve('./www/bots.html');
	win.loadFile(startingPoint);


	// win.webContents.openDevTools();
	
	win.on('close', (e) => {
		if (!app.isQuiting) {
	        e.preventDefault();
	        win.hide();
	    }

	    return false;
	});
	win.on('closed', () => {
		win = null;
	});
	
	createTray();
}

function createTray(){
	tray = new Tray('./www/img/icon_256.png');
	const contextMenu = Menu.buildFromTemplate([
		{
			label: 'Open perbotio', 
			click: function(){
				win.show();
			} 
		},
		{
			label: 'Attendance', 
			click: function(){
				const robot = new perbotio('attendance', true);
				robot.run();
			}
		},
		{
			label: 'Fix Month', 
			click: function(){
				const robot = new perbotio('fixMonth', true);
				robot.run();
			}
		},
		{
			label: 'Quit', 
			click: function(){
				app.isQuiting = true;
				app.quit();
			}
		}
	])
	tray.setToolTip('PerBOTio')
	tray.setContextMenu(contextMenu)
}

app.on('ready', createWindow);
app.on('window-all-closed', () => {
	if( process.platform !== 'darwin' ){
		app.quit();
	}
});
app.on('activate', () => {
	if( win === null ){
		createWindow();
	}
});



ipcMain.on('runScraper', async(event, data) => {
	data = JSON.parse(data);

	var bot = data.scraper;
	var headlessMode = data?.headless ?? true;

	const robot = new perbotio(bot, headlessMode);
	if (data.monthProcess) {
		await robot.setMonth(data.monthProcess - 1);
	}
	await robot.run();

	event.reply('finishedScraper', bot)
});