const core = require('./core.js');
const dateFormat = require('dateformat');


class fixMonth extends core {
	constructor(){
		super();

		this.botName = 'perbotiofixMonth';
	}

	async getWeekDates(){
		Date.prototype.addDays = function(days) {
			var date = new Date(this.valueOf());
			date.setDate(date.getDate() + days);
			return date;
		}

		var endDate = new Date();
		if (endDate.getMonth() != this.monthProcess) {
			endDate.setMonth(this.monthProcess);
			endDate.setDate(new Date(endDate.getFullYear(), this.monthProcess + 1, 0).getDate());
		}
		var startDate = new Date(dateFormat(endDate, 'yyyy-mm-01'));

		var dates = [];
		var currentDate = startDate;
		do{
			if( new Date(currentDate).getDay() > 5 || new Date(currentDate).getDay() < 1 ){ continue; }

			dates.push(new Date(currentDate));
		} while( (currentDate = currentDate.addDays(1)) && currentDate <= endDate );

		// Change month if we are fixing something in the past
		if (this.monthProcess != (new Date).getMonth()) {
			await this.changeMonth();
		}

		return dates;
	}

	async changeMonth(){
		var monthSelector = await this.page.waitForSelector('[data-test-id="month-picker-input"]', {visible: true}).catch(() => { return false; });
		if (!monthSelector) { return this.error('No month selector'); }
		await this.page.click('[data-test-id="month-picker-input"]');

		var monthSelector = await this.page.waitForSelector('.flatpickr-monthSelect-month', {visible: true}).catch(() => { return false; });
		if (!monthSelector) { return this.error('No month selector 2'); }

		await this.page.evaluate((monthProcess) => {
			document.querySelectorAll('.flatpickr-monthSelect-month')[monthProcess].click();
		}, this.monthProcess); 

		await this.page.waitForTimeout(1000);
	}
}
module.exports = fixMonth;