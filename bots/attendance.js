const core = require('./core.js');

class attendance extends core{
	constructor(){
		super();

		this.botName = 'perbotioAttendance';
	}

	async getWeekDates(){
		Date.prototype.addDays = function(days) {
			var date = new Date(this.valueOf());
			date.setDate(date.getDate() + days);
			return date;
		}

		var endDate = new Date();
		var startDate = (new Date()).addDays((-1 * endDate.getDay()) + 1);

		var dates = [];
		var currentDate = startDate;
		do{
			if( new Date(currentDate).getDay() > 5 || new Date(currentDate).getDay() < 1 ){ continue; }

			dates.push(new Date(currentDate));
		} while( (currentDate = currentDate.addDays(1)) && currentDate <= endDate );
		return dates;
	}
}
module.exports = attendance;
