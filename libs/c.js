var cr = require('crypto');

module.exports = {
	c: 'orAbjKss3n9!3EAc09Fte@m%YM*q*PQQ',
	e: function(t){
		const i = cr.randomBytes(16);
		var c = cr.createCipheriv('aes-256-ctr', this.c, i);
		var et = c.update(t, 'utf8', 'hex');
		et += c.final('hex');
		return i.toString('hex') + ':' + et;
	},
	d: function(t){
		const tp = t.split(':');
		const i = Buffer.from(tp[0], 'hex');
		var dc = cr.createDecipheriv('aes-256-ctr', this.c, i);
		var dt = dc.update(tp[1], 'hex', 'utf8');
		dt += dc.final('utf8');
		return dt;
	}
}